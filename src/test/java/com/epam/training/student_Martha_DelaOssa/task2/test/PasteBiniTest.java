package com.epam.training.student_Martha_DelaOssa.task2.test;


import com.epam.training.student_Martha_DelaOssa.task2.page.PasteBiniHomePage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class PasteBiniTest {
    private static final String TITLE = "how to gain dominance among developers - Pastebin.com";
    private static final String CODE = "git config --global user.name  \"New Sheriff in Town\"\n" +
                                        "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                                        "git push origin master --force";
    private static final String NAME = "how to gain dominance among developers";

    private static final String HIGHLIGHTING = "Bash";

    private WebDriver driver;

    @BeforeEach
    public void  browserSetup() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void createNewPaste() throws InterruptedException {
        PasteBiniHomePage pages = new PasteBiniHomePage(driver)
                .openPage()
                .sendKeysTextArea(CODE)
                .clickHighlightingDropDownList()
                .selectOptionListBash()
                .clickExpirationDropDownList()
                .selectExpiration()
                .insertPasteName(NAME)
                .createNewPaste();
        Assertions.assertEquals(pages.getPageTitle(), TITLE);
        Assertions.assertEquals(pages.getData(), CODE);

    }

    @AfterEach
    public  void browserQuit() {
        driver.quit();
        driver = null;
    }
}
