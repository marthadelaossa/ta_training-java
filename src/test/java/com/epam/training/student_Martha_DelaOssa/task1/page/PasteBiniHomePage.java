package com.epam.training.student_Martha_DelaOssa.task1.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class PasteBiniHomePage {

    private static final String HOMEPAGE_URL = "https://pastebin.com/";
    public static final Duration WAIT_TIME = Duration.ofSeconds(10);

    @FindBy(id = "postform-text")
    public WebElement textArea;

    @FindBy (id = "select2-postform-expiration-container")
    public WebElement expirationDropDownList;

    @FindBy (xpath = "//li[contains(text(), '10 Minutes')]")
    private WebElement optionList;

    @FindBy (id = "//ol [@class='bash']")
    private WebElement namePostformInput;

    @FindBy (xpath = "//button[@class='btn -big']")
    private WebElement createNewPasteButton;

    protected final WebDriver driver;
    public WebDriverWait wait;

    public PasteBiniHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public PasteBiniHomePage openPage() {
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver, WAIT_TIME).until(CustomConditions.jQueryAJAXsCompleted());
        return new PasteBiniHomePage(driver);
    }
    public static void waitForElementToBeClickable(WebElement element, WebDriver driver){
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public PasteBiniHomePage sendKeysTextArea (String text) {
        waitForElementToBeClickable(textArea, driver);
        textArea.sendKeys(text);
        return this;
    }

    public PasteBiniHomePage clickExpirationDropDownList() {
        waitForElementToBeClickable(expirationDropDownList, driver);
        expirationDropDownList.click();
        return this;
    }

    public PasteBiniHomePage selectExpiration() {
        waitForElementToBeClickable(optionList, driver);
        optionList.click();
        return this;
    }

    public PasteBiniHomePage insertPasteName(String name) {
        waitForElementToBeClickable(namePostformInput, driver);
        namePostformInput.sendKeys(name);
        return this;
    }

    public PasteBiniHomePage createNewPaste () {
        waitForElementToBeClickable(createNewPasteButton, driver);
        createNewPasteButton.click();
        return this;
    }

    public String getPageTitle() throws InterruptedException {
        Thread.sleep(2000);
        return driver.getTitle();
    }

}
