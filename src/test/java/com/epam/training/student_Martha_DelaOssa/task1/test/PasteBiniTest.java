package com.epam.training.student_Martha_DelaOssa.task1.test;



import com.epam.training.student_Martha_DelaOssa.task1.page.PasteBiniHomePage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class PasteBiniTest {
    private static final String TITLE = "helloweb - Pastebin.com";
    private static final String CODE = "Hello from WebDriver";
    private static final String NAME = "helloweb";

    private WebDriver driver;

    @BeforeEach
    public void  browserSetup() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void createNewPaste() throws InterruptedException {
        PasteBiniHomePage pages = new PasteBiniHomePage(driver)
                .openPage()
                .sendKeysTextArea(CODE)
                .clickExpirationDropDownList()
                .selectExpiration()
                .insertPasteName(NAME)
                .createNewPaste();
        Assertions.assertEquals(pages.getPageTitle(), TITLE);

    }

    @AfterEach
    public  void browserQuit() {
        driver.quit();
        driver = null;
    }
}
