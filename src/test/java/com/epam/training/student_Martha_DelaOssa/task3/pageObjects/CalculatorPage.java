package com.epam.training.student_Martha_DelaOssa.task3.pageObjects;

import org.openqa.selenium.By;


public class CalculatorPage {

    public  By BTN_ADD_ESTIMATE = By.xpath("//button[child::*[contains(text(),'Add to estimate')]]");
    public  By BTN_COMPUTE_ENGINE = By.xpath("//*[@tabindex='0'][child::*[text()='Compute Engine']]");

    public  By BTN_ADD_TO_ESTIMATE = By.xpath("//*[@class='jirROd'][descendant::*[text()='Add to estimate']]");
    public  By INPUT_NUMBER_INSTANCES = By.xpath("//input[@class='qdOxv-fmcmS-wGMbrd'][ancestor::*[@class='QiFlid']]");

    public  By LST_OPERATING_SYSTEM = By.xpath("(//*[@class='VfPpkd-aPP78e'])[4]");
    public  By UL_OPERATING_SYSTEM = By.xpath("//li[@tabindex='0']");

    public  By BTN_REGULAR = By.xpath("//*[child::*[text()='Regular']]");

    public  By LST_MACHINE_FAMILY = By.xpath("(//*[@class='VfPpkd-aPP78e'])[5]");
    public  By UL_MACHINE_FAMILY = By.xpath("//li[@data-value='general-purpose']");

    public  By LST_SERIES = By.xpath("(//*[@class='VfPpkd-aPP78e'])[6]");
    public  By UL_SERIES = By.xpath("//li[@data-value='n1']");

    public  By LST_MACHINE_TYPE = By.xpath("(//*[@class='VfPpkd-aPP78e'])[7]");
    public  By UL_MACHINE_TYPE = By.xpath("//li[@data-value='n1-standard-8']");

    public  By BTN_ADD_GPUS = By.xpath("//span[@class='eBlXUe-hywKDc'][ancestor::*[@aria-label='Add GPUs']]");

    public  By LST_GPU_MODEL = By.xpath("(//*[@class='VfPpkd-aPP78e'])[8]");
    public  By UL_GPU_MODEL = By.xpath("//li[@data-value='nvidia-tesla-v100']");

    public  By LST_NUMBERS_GPU= By.xpath("(//*[@class='VfPpkd-aPP78e'])[9]");
    public  By UL_NUMBERS_GPU = By.xpath("(//li[@data-value='1'])[1]");

    public  By LST_LOCAL_SSD= By.xpath("(//*[@class='VfPpkd-aPP78e'])[10]");
    public  By UL_LOCAL_SSD = By.xpath("(//li[@data-value='2'])[2]");

    public  By LST_REGION= By.xpath("(//*[@class='VfPpkd-aPP78e'])[11]");
    public  By UL_REGION = By.xpath("//li[@data-value='us-central1']");

    public  By BTN_COMMITED_USE = By.xpath("//*[child::*[text()='1 year']]");

    public  By COMODIN= By.xpath("(//*[@class='VfPpkd-aPP78e'])[13]");

    public  By BTN_CLOSE_ADD_ESTIMATE= By.xpath("//button[@aria-label='Cerrar cuadro de diálogo']");

    public  By BTN_SHARE= By.xpath("//button[@aria-label='Open Share Estimate dialog']");

    public  By BTN_OPEN_SUMARY= By.xpath("//a[@track-name='open estimate summary']");

}
