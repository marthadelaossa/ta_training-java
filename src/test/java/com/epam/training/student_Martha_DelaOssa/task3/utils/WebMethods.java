package com.epam.training.student_Martha_DelaOssa.task3.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class WebMethods {

     public static WebDriver driver;
     public static WebDriverWait wait;


     public WebMethods(WebDriver driver, WebDriverWait wait) {
          this.driver = driver;
          this.wait = new WebDriverWait(driver, Duration.ofMillis(4000));
     }


     public static void url(String url) throws InterruptedException {
          driver.get(url);
          Thread.sleep(1000);
     }

     public static WebElement findElement(By locator) {
          wait.until(ExpectedConditions.presenceOfElementLocated(locator));
          return driver.findElement(locator);
     }

     public static  boolean elementVisible(By locator) {
          JavascriptExecutor js = (JavascriptExecutor) driver;
          js.executeScript("arguments[0].scrollIntoView(true);",  wait.until(ExpectedConditions.presenceOfElementLocated(locator)));
          return driver.findElement(locator).isDisplayed();

     }

     public static void sendKey(CharSequence pressKeys, By locator) {
          wait.until(ExpectedConditions.presenceOfElementLocated(locator));
          findElement(locator).sendKeys(pressKeys);
     }

     public static void clearInput(By locator) {
          wait.until(ExpectedConditions.presenceOfElementLocated(locator));
          findElement(locator).clear();
     }

     public static void  click (By locator) {
          wait.until(ExpectedConditions.elementToBeClickable(locator));
          findElement(locator).click();
     }

     public static void enter(By locator) {
          wait.until(ExpectedConditions.elementToBeClickable(locator));
          findElement(locator).submit();
     }
     public static void  scrollTop () {
          JavascriptExecutor js = (JavascriptExecutor) driver;
           js.executeScript("window.scrollTo(0, -document.body.scrollHeight)");

     }

     public static void  scrollToButton (By locator) {
          JavascriptExecutor js = (JavascriptExecutor) driver;
          js.executeScript("arguments[0].scrollIntoView(true);",  wait.until(ExpectedConditions.elementToBeClickable(locator)));
          js.executeScript("arguments[0].click();",  wait.until(ExpectedConditions.elementToBeClickable(locator)));

     }

     public String getData(By locator) {
          return findElement(locator).getText();
     }
     public void validarMsn(By locator) {
          wait.until(ExpectedConditions.presenceOfElementLocated(locator));
     }
     public void close() {
          driver.quit();
     }


}