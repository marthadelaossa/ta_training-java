package com.epam.training.student_Martha_DelaOssa.task3.test;


import com.epam.training.student_Martha_DelaOssa.task3.driver.ChromeDriveOptions;
import com.epam.training.student_Martha_DelaOssa.task3.pageObjects.CalculatorPage;
import com.epam.training.student_Martha_DelaOssa.task3.pageObjects.HomePage;
import com.epam.training.student_Martha_DelaOssa.task3.pageObjects.ResultPage;
import com.epam.training.student_Martha_DelaOssa.task3.pageObjects.SummaryPage;
import com.epam.training.student_Martha_DelaOssa.task3.utils.WebMethods;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoogleCloudTest {
    private static final String TEXT_TO_SEARCH = "Google Cloud Platform Pricing Calculator";

    private static final String NUMBER_INSTANCES = "4";
    private static final String HOMEPAGE_URL=" https://cloud.google.com/";
    public static WebDriver driver;
    public static WebDriverWait wait;
    static ChromeDriveOptions chromeDriveOptions=new ChromeDriveOptions();

    HomePage homePage= new HomePage();
    ResultPage resultPage=new ResultPage();

    SummaryPage summaryPage= new SummaryPage();

    CalculatorPage calculatorPage=new CalculatorPage();

    @BeforeAll
    public static void browserSetup() throws InterruptedException {
        driver = new ChromeDriver(chromeDriveOptions.options());
        WebMethods webMethods= new WebMethods (driver, wait);
        WebMethods.url(HOMEPAGE_URL);
    }

    @Test
    @Order(1)
    public void searchText() {
        WebMethods.findElement(homePage.INPUT_SEARCH_BOX);
        WebMethods.sendKey(TEXT_TO_SEARCH,homePage.INPUT_SEARCH_BOX);
        WebMethods.enter(homePage.INPUT_SEARCH_BOX);
    }

    @Test
    @Order(2)
    public void clickResult() {
        WebMethods.findElement(resultPage.LINK_SEARCH_RESULT);
        WebMethods.click(resultPage.LINK_SEARCH_RESULT);
    }

    @Test
    @Order(3)
    public void calculator() {
        WebMethods.findElement(calculatorPage.BTN_ADD_ESTIMATE);
        WebMethods.click(calculatorPage.BTN_ADD_ESTIMATE);
        WebMethods.click(calculatorPage.BTN_COMPUTE_ENGINE);
        WebMethods.findElement(calculatorPage.BTN_ADD_TO_ESTIMATE);
        WebMethods.clearInput(calculatorPage.INPUT_NUMBER_INSTANCES);
        WebMethods.sendKey(NUMBER_INSTANCES,calculatorPage.INPUT_NUMBER_INSTANCES);

        WebMethods.click(calculatorPage.LST_OPERATING_SYSTEM);
        WebMethods.click(calculatorPage.UL_OPERATING_SYSTEM);

        WebMethods.click(calculatorPage.BTN_REGULAR);

        WebMethods.click(calculatorPage.LST_MACHINE_FAMILY);
        WebMethods.click(calculatorPage.UL_MACHINE_FAMILY);

        WebMethods.click(calculatorPage.LST_SERIES);
        WebMethods.click(calculatorPage.UL_SERIES);

        WebMethods.click(calculatorPage.LST_MACHINE_TYPE);
        WebMethods.click(calculatorPage.UL_MACHINE_TYPE);

        WebMethods.click(calculatorPage.BTN_ADD_GPUS);

        WebMethods.findElement(calculatorPage.COMODIN);
        WebMethods.click(calculatorPage.LST_GPU_MODEL);
        WebMethods.click(calculatorPage.UL_GPU_MODEL);

        WebMethods.click(calculatorPage.LST_NUMBERS_GPU);
        WebMethods.click(calculatorPage.UL_NUMBERS_GPU);

        WebMethods.click(calculatorPage.LST_LOCAL_SSD);
        WebMethods.click(calculatorPage.UL_LOCAL_SSD);

        WebMethods.click(calculatorPage.LST_REGION);
        WebMethods.click(calculatorPage.UL_REGION);

        WebMethods.click(calculatorPage.BTN_COMMITED_USE);

        WebMethods.scrollTop();

        WebMethods.click(calculatorPage.BTN_ADD_ESTIMATE);

        WebMethods.scrollToButton(calculatorPage.BTN_SHARE);

        WebMethods.click(calculatorPage.BTN_OPEN_SUMARY);

    }

    @Test
    @Order(4)
    public void summary()  {
        Assertions.assertTrue( WebMethods.elementVisible(summaryPage.LBL_ESTIMATE_COST),"Product Cost in not correct");
    }

    @AfterAll
    public static void browserQuit() {
        driver.quit();
        driver = null;
    }
}
